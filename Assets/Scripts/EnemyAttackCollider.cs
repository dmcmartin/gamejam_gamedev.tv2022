using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttackCollider : MonoBehaviour
{
    public bool isAttacking=false;


    private void OnTriggerEnter2D(Collider2D other) {
        // Debug.Log("Enemy trigger: other.tag = "+other.tag + ", isAttacking = "+isAttacking);

        if (!isAttacking) return;


        if (other.tag == "Player") {
            // Debug.Log("HIT");
            HitSystem hitSystem = other.GetComponent<HitSystem>();
            hitSystem.OnHit(this.gameObject);



            // HitSystem MyhitSystem = gameObject.transform.parent.transform.parent.GetComponent<HitSystem>();
            // Debug.Log("MyhitSystem = " + MyhitSystem);


            // PossessedEnemyController possessedEnemyController = GetComponent<PossessedEnemyController>();
            // Debug.Log("possessedEnemyController = " + possessedEnemyController);

            // CrawlingAlienMonster_Movement crawler_movement = GetComponent<CrawlingAlienMonster_Movement>();
            // Debug.Log("crawler_movement = " + crawler_movement);

        } else if (other.tag == "EnemySprite") {
            // Debug.Log("Collide = " + other.tag + ": "+other.transform.parent.tag+ " other = "+other);

            //Well...this code has lost all generalization...I dont' care, just want it done
            PossessedEnemyController possessedEnemyController = gameObject.transform.parent.transform.parent.GetComponent<PossessedEnemyController>();
            // Debug.Log("possessedEnemyController = " + possessedEnemyController);

            if (possessedEnemyController.GetIsPossessed()) {
                // Debug.Log("Attack!");
                //The player is possessing "this" object, and is attacking another enemy
                HitSystem hitSystem = other.transform.parent.GetComponent<HitSystem>();
                hitSystem.OnHit(this.gameObject);
            }




        }
    }

}
