using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//This class is used to scroll the different levels of the scene in a way that 
//mimics the effect of parallax

//TODO: Really could use refactoring to use this with generic scenes

public class ParallaxScroller : MonoBehaviour
{


    bool DEBUG_MOVE_MARKERS = false;


    //Variables associated with the sky background
    BoundsCalculator [] backgroundSky_BCalcs;
    float backgroundSkySpeedFactor =  0.025f;

    //Variables associated with the Far-range set of mountains
    BoundsCalculator [] farMountains_BCalcs;
    float farMountainSpeedFactor =  0.05f;


    //Variables associated with the Mid-range set of mountains
    BoundsCalculator [] midMountains_BCalcs;
    float midMountainSpeedFactor =  0.15f;


    //Variables associated with the forground
    GameObject [] foregroundObjects;
    BoundsCalculator [] foreground_BCalcs;
    float foregroundSpeedFactor =  1.0f; //1.0 actually means it stays still






    // Start is called before the first frame update
    void Start()
    {
        //NOTE: This class only supports 2 tiles per layer
        //Specify the Game Objects that are collections that will be handled in each specific layer
        GameObject[] tilesInLayer;


        tilesInLayer = new GameObject[] {
            GameObject.Find("BackgroundSky1"),
            GameObject.Find("BackgroundSky2")
        };
        backgroundSky_BCalcs = DefineTileLayer(tilesInLayer);



        tilesInLayer = new GameObject[] {
            GameObject.Find("BackgroundMountainsFar1"),
            GameObject.Find("BackgroundMountainsFar2")
        };
        farMountains_BCalcs = DefineTileLayer(tilesInLayer);

        tilesInLayer = new GameObject[] {
            GameObject.Find("BackgroundMountainsMid1"),
            GameObject.Find("BackgroundMountainsMid2")
        };
        midMountains_BCalcs = DefineTileLayer(tilesInLayer);

        tilesInLayer = new GameObject[] {
            GameObject.Find("Foreground1"),
            GameObject.Find("Foreground2")
        };
        foreground_BCalcs = DefineTileLayer(tilesInLayer);



    }

    private BoundsCalculator[] DefineTileLayer(GameObject[] tilesInLayer)
    {
        bool allTilesFound = true;
        BoundsCalculator[] boundsCalc = new BoundsCalculator[tilesInLayer.Length];
        for (int i = 0; i < tilesInLayer.Length; i++)
        {
            // Debug.Log("item " + i + " = " +boundsCalc[i]);

            if (tilesInLayer[i] == null){
                allTilesFound = false;
                break;
            }

            boundsCalc[i] = tilesInLayer[i].GetComponent<BoundsCalculator>();
        }
        

        if (!allTilesFound) {
            boundsCalc = null;
        }

        return boundsCalc;
    }



    //Called by the FollowCamera class to indicate that the camera has moved location
    public void OnCameraMoved(float xPos)
    {

        SlideBackground(backgroundSky_BCalcs, backgroundSkySpeedFactor, xPos);
        SlideBackground(farMountains_BCalcs, farMountainSpeedFactor, xPos);
        SlideBackground(midMountains_BCalcs, midMountainSpeedFactor, xPos);
        SlideBackground(foreground_BCalcs, foregroundSpeedFactor, xPos);


        if (DEBUG_MOVE_MARKERS){
            GameObject marker = GameObject.Find("Marker");
            marker.transform.position = midMountains_BCalcs[0].bounds().center;
        }


    }

    void SlideBackground(BoundsCalculator [] boundsCalculators, float speedFactor, float xPos)
    {
        //NOTE: The tiles are not assumed to be equal sizes
        //NOTE: It is assumed that there are only 2 tiles

        // Debug.Log("Start");

        if (boundsCalculators == null) {
            return;
        }

        int numTiles = boundsCalculators.Length;
        float allTileWidth = 0;

        // Debug.Log("Stop");



        //Tile centers as if the tiles were located at the origin 
        float [] normalizedTileCenters = new [] { 
            boundsCalculators[0].bounds().extents.x,
            boundsCalculators[0].bounds().size.x + boundsCalculators[1].bounds().extents.x
         };

        if (DEBUG_MOVE_MARKERS) {
            Vector3 tempPos;

            GameObject Tile1EndMarker = GameObject.Find("Tile1EndMarker");
            tempPos = Tile1EndMarker.transform.position;
            tempPos.x = boundsCalculators[0].bounds().size.x;
            Tile1EndMarker.transform.position = tempPos;

            GameObject Tile2EndMarker = GameObject.Find("Tile2EndMarker");
            tempPos = Tile2EndMarker.transform.position;
            tempPos.x = boundsCalculators[0].bounds().size.x + boundsCalculators[1].bounds().size.x;
            Tile2EndMarker.transform.position = tempPos;

            GameObject Tile1MidMarker = GameObject.Find("Tile1MidMarker");
            tempPos = Tile1MidMarker.transform.position;
            tempPos.x = boundsCalculators[0].bounds().extents.x;
            Tile1MidMarker.transform.position = tempPos;

            GameObject Tile2MidMarker = GameObject.Find("Tile2MidMarker");
            tempPos = Tile2MidMarker.transform.position;
            tempPos.x = boundsCalculators[0].bounds().size.x + boundsCalculators[1].bounds().extents.x;
            Tile2MidMarker.transform.position = tempPos;
        }

        //Calculate the width of the tiles combined
        foreach (BoundsCalculator boundsCalc in boundsCalculators) {
            allTileWidth += boundsCalc.bounds().size.x;
        }
        //Backgrounds that are far away need to "follow" the player, so they move with the player, but at a slower speed
        float virtualXOffset = xPos *(1.0f -  speedFactor);  
        //Offset of Player from the "start" of the tile.  The start has been shifted to follow the player by a factor (speedFactor)
        float visualOffsetIntoScene = xPos - virtualXOffset; 


        //
        float VisualWrappedXOffset = (visualOffsetIntoScene>=0) ? 
            visualOffsetIntoScene % allTileWidth :
            allTileWidth + (visualOffsetIntoScene % allTileWidth) //For negative ranges, we still want the wrapped offset to be positive
            ;
        float numWraps = Mathf.Floor(visualOffsetIntoScene / allTileWidth);


        if (DEBUG_MOVE_MARKERS) {
            Vector3 tempPos;

            GameObject marker1 = GameObject.Find("Marker1");
            tempPos = marker1.transform.position;
            tempPos.x = virtualXOffset + 0;
            marker1.transform.position = tempPos;

            GameObject marker2 = GameObject.Find("Marker2");
            tempPos = marker1.transform.position;
            tempPos.x = VisualWrappedXOffset;
            marker2.transform.position = tempPos;
        }





        BoundsCalculator [] sortedBoundsCalculators = new BoundsCalculator [numTiles]; //We rearrange the tiles based on proximity to the player

        //Three Regions:  //TODO: need better way to describe
        //  player before center of 1st tile, after center of 2nd tile that has been wrapped to the left of the player
        //  player after center of 1st tile, before center of 2nd tile, no wrapping
        //  player after center of 1st tile, after center of 2nd tile, 1st tile wrapped to the right of player
        float additionalVirtualOffset = 0;
        if (VisualWrappedXOffset < normalizedTileCenters[0]  ) {
            //Region 1: Before 1st tile's center
            //2nd Tile gets moved to the left of 1st tile
            sortedBoundsCalculators[0] = boundsCalculators[1];
            sortedBoundsCalculators[1] = boundsCalculators[0];
            additionalVirtualOffset = -boundsCalculators[1].bounds().size.x;
            // Debug.Log("In Region 1:  Tile 2 swapped to the left");
        } else if (VisualWrappedXOffset > normalizedTileCenters[0]  && VisualWrappedXOffset < normalizedTileCenters[1]){
            //No Swapping of tiles
            sortedBoundsCalculators[0] = boundsCalculators[0];
            sortedBoundsCalculators[1] = boundsCalculators[1];
            additionalVirtualOffset = 0;
            // Debug.Log("In Region 2:  No Swapping");
        } else { //VisualWrappedXOffset > Tile2.center
            //1st tile gets moved to the right of 2nd tile
            sortedBoundsCalculators[0] = boundsCalculators[1];
            sortedBoundsCalculators[1] = boundsCalculators[0];
            additionalVirtualOffset = boundsCalculators[0].bounds().size.x;
            // Debug.Log("In Region 3:  Tile 1 swapped to the right");

        }



        // Debug.Log("virtualXOffset = " + virtualXOffset + " , additionalVirtualOffset = "+ additionalVirtualOffset);


        //Translate each tile to the locations needed
        float cumulativeTileOffset = additionalVirtualOffset + numWraps*allTileWidth;
        for (int i = 0; i< numTiles; i++) {
            sortedBoundsCalculators[i].SetLeftPositionX(virtualXOffset + cumulativeTileOffset);
            cumulativeTileOffset += sortedBoundsCalculators[i].bounds().size.x; 

            //This hack is not working:
            //This is a fudge factor to ensure there is overlap in the tiles.  
            //Without this, there are sometimes a very think line of pixels that are blank
            //between the tiles.  This may be due to rounding affects.
            // cumulativeTileOffset -= 0.4f;  

            //There seems to be some glitch between tiles


        }







    }


}
