using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] Animator animator;
    [SerializeField] float speed = 15f; 
    [SerializeField]  GameObject prefabGhost;
    [SerializeField]  GameObject prefabFireball;
    float curXPos=0;
    bool facingLeft = false;

    bool isMoving=false;

    public bool isDead = false;
    // bool ghostIsRising = false;


    // Start is called before the first frame update
    void Start()
    {
        transform.position = new Vector3(curXPos,transform.position.y,transform.position.z);

        HitSystem hitSystem = GetComponent<HitSystem>();
        hitSystem.OnDeadEvent.AddListener(OnDead);
        hitSystem.OnReviveEvent.AddListener(OnRevive);
        hitSystem.OnHitEvent.AddListener(OnHit);

    }

    // Update is called once per frame
    void Update()
    {
        isMoving = false;

        // if (ghostIsRising) {
        //     RiseGhost();
        //     return;
        // }

        if (isDead) {
            return;
        }

        if (Input.GetKeyDown(KeyCode.Space)) {
            Attack();
        }


        float moveAmount = Input.GetAxis("Horizontal") * speed * Time.deltaTime;
        transform.Translate(moveAmount,0,0);        
        isMoving = Mathf.Abs(moveAmount) > 0;

        if (isMoving) {
            facingLeft = moveAmount < 0;
        }

        animator.SetBool("IsWalking", isMoving);
        Vector3 newScale = transform.localScale;
        newScale.x = Mathf.Abs(newScale.x) * (facingLeft ? -1f : 1f );
        transform.localScale = newScale;



        // //DEBUG:
        // if (Input.GetKeyUp(KeyCode.Space)) {
        //     HitSystem hitSystem = GetComponent<HitSystem>();
        //     hitSystem.OnHit(null);
        //     // Debug.Log("Perform Hit");
        // }
    }

    void OnDead() {
        Debug.Log("Player is Dead");
        isDead = true;
        // animator.SetTrigger("Die");
        animator.SetBool("IsDead",true);

        GameObject ghostPlayer = Instantiate(prefabGhost, new Vector3(transform.position.x, transform.position.y,0),Quaternion.identity);
        GhostPlayerController ghostController = ghostPlayer.GetComponent<GhostPlayerController>();
        ghostController.RiseGhost(gameObject);


        // GameObject ghostPlayer = Instantiate(prefabGhost, new Vector3(transform.position.x, transform.position.y + ghostPositionAbovePlayer,0),Quaternion.identity);
        // GhostPlayerController ghostController = ghostPlayer.GetComponent<GhostPlayerController>();
        // ghostController.SetPlayer(gameObject);

    }

    void OnRevive() {
        // Debug.Log("It's Alive!");
        isDead = false;
        animator.SetBool("IsDead",false);

    }


    // void OnCollisionEnter2D(Collision2D other) {
    //     Debug.Log("Player collide");
    // }

    // private void OnTriggerEnter2D(Collider2D other) {
    //     Debug.Log("Player trigger");
    // }

    void Attack() {
        isMoving = false;
        // isAttacking = true;
        animator.SetTrigger("Attack");
    }
    void OnHit() {
        Debug.Log("Hit");
        isMoving = false;
        animator.SetTrigger("Hit");
    }
    void LaunchRangedWeapon() {

        Vector3 fireballPos = transform.position;
        if (facingLeft) {
            fireballPos.x -= 0.8f;
        } else {
            fireballPos.x += 0.8f;
        }
        fireballPos.y += 0.15f;

        GameObject fireball = Instantiate(prefabFireball, fireballPos,Quaternion.identity);
        FireballController fireballController = fireball.GetComponent<FireballController>();
        fireballController.Launch(facingLeft);

    }

}
