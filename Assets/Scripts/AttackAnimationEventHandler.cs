using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AttackAnimationEventHandler : MonoBehaviour
{
    public bool isAttacking = false;
    public UnityEvent OnAttackAnimStartEvent = new UnityEvent();
    public UnityEvent OnAttackAnimStopEvent = new UnityEvent();
    public UnityEvent OnDieAnimStopEvent = new UnityEvent();
    // public UnityEvent OnDieAnimStartEvent = new UnityEvent();


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnAttackAnimStart(){
        // Debug.Log("event: OnAttackAnimStart");
        isAttacking = true;
        OnAttackAnimStartEvent.Invoke();
    }
    public void OnAttackAnimStop(){
        // Debug.Log("event: OnAttackAnimStop");
        isAttacking = false;        
        OnAttackAnimStopEvent.Invoke();
    }
    public void OnDieAnimStart(){
        // Debug.Log("event: OnDieAnimStart");
        // OnDieAnimStartEvent.Invoke();
    }
    public void OnDieAnimStop(){
        // Debug.Log("event: OnDieAnimStop");
        OnDieAnimStopEvent.Invoke();
    }


}
