using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorfulBee_Movement : MonoBehaviour
{
    [SerializeField] Animator animator;
    [SerializeField] float playerProximityDistance = 15f;  //Distance to player threshold to trigger walking towards player
    float playerProximityDistanceSqr ; //Precalculation of the distance squared threshold to trigger walking towards player
    float attackDistance = 3.0f; //Distance away before enemy attacks player
    float attackDistanceSqr;
    [SerializeField] float movementSpeed = 4.5f;
    bool isMoving = false;
    // bool isAttacking = false;
    bool facingLeft = true;

    bool isPossessed = false;


    // Start is called before the first frame update
    void Start()
    {
        playerProximityDistanceSqr = playerProximityDistance*playerProximityDistance; 
        attackDistanceSqr = attackDistance*attackDistance;

        PossessedEnemyController possessedEnemyController = GetComponent<PossessedEnemyController>();
        possessedEnemyController.OnTakePossessionEvent.AddListener(OnTakePossession);
        possessedEnemyController.OnReleasePossessionEvent.AddListener(OnReleasePossession);

        HitSystem hitSystem = GetComponent<HitSystem>();
        hitSystem.OnDeadEvent.AddListener(OnDead);

        AttackAnimationEventHandler attackEventHandler =  GetComponentInChildren(typeof(AttackAnimationEventHandler)) as AttackAnimationEventHandler;
        attackEventHandler.OnAttackAnimStartEvent.AddListener(OnAttackAnimStart);
        attackEventHandler.OnAttackAnimStopEvent.AddListener(OnAttackAnimStop);
        attackEventHandler.OnDieAnimStopEvent.AddListener(OnDieAnimStop);

    }

    
    // Update is called once per frame
    void Update()
    {

        // isAttacking = false;
        isMoving = false;

        if (isPossessed) {
            float moveAmount = Input.GetAxis("Horizontal") * movementSpeed * Time.deltaTime;
            transform.Translate(moveAmount,0,0);     
            isMoving = Mathf.Abs(moveAmount) > 0;

            if (isMoving) {
                facingLeft = moveAmount < 0;
            }

            // if (Input.GetMouseButtonDown(0))
            // {
            //     isMoving = false;
            //     // isAttacking = true;
            //     animator.SetTrigger("Attack");
            // }

            if (Input.GetKeyDown(KeyCode.Space)) {
                isMoving = false;
                // isAttacking = true;
                animator.SetTrigger("Attack");
            }


        } else {

            GameObject player = GameSystem.Instance.player;

            Vector3 vector2Player = player.transform.position - transform.position;
            // if (vector2Player.sqrMagnitude <= attackDistanceSqr) {
            if (Mathf.Abs(vector2Player.x) <= attackDistance) {
                PlayerController playerController = player.GetComponent<PlayerController>();
                if (!playerController.isDead) {
                    animator.SetTrigger("Attack");
                    isMoving = false;
                }

            }else if (vector2Player.sqrMagnitude <= playerProximityDistanceSqr) {
                //Move Enemy towards character
                isMoving = true;

                Vector3 target_pos = new Vector3(player.transform.position.x, transform.position.y, transform.position.z);

                transform.position = Vector3.MoveTowards(transform.position, target_pos, movementSpeed * Time.deltaTime);

                facingLeft = vector2Player.x < 0;

            }


            // if ( (vector2Player.x >= 0 && facingLeft) || 
            //     (vector2Player.x < 0 && !facingLeft) ) {
            //     Vector3 newScale = transform.localScale;
            //     newScale.x *= -1;
            //     transform.localScale = newScale;
            //     facingLeft = !facingLeft;
            // }
        }

        Vector3 newScale = transform.localScale;
        newScale.x = Mathf.Abs(newScale.x) * (facingLeft ? 1f : -1f );
        transform.localScale = newScale;


        // animator.SetBool("IsWalking", isMoving);

        // Debug.Log("vector2Player.sqrMagnitude = " + vector2Player.sqrMagnitude + "playerProximityDistanceSqr = " + playerProximityDistanceSqr);


    }
    public void OnTakePossession(){
        Debug.Log("IsPossessed");
        isPossessed = true;
    }
    public void OnReleasePossession(){
        Debug.Log("No LongerPossessed");
        isPossessed = false;
    }
    // public bool GetIsPossessed(){
    //     return isPossessed;
    // }
    void OnDead() {
        animator.SetTrigger("Die");
        // Destroy(gameObject);
    }
    public void OnAttackAnimStart(){
        // isAttacking = true;

        EnemyAttackCollider attackCollider =  GetComponentInChildren(typeof(EnemyAttackCollider)) as EnemyAttackCollider;

        // EnemyAttackCollider attackCollider = gameObject.GetComponent<EnemyAttackCollider>();
        attackCollider.isAttacking = true;
        // Debug.Log("OnAttackAnimStart");
    }
    public void OnAttackAnimStop(){
        // isAttacking = false;
        EnemyAttackCollider attackCollider =  GetComponentInChildren(typeof(EnemyAttackCollider)) as EnemyAttackCollider;
        // EnemyAttackCollider attackCollider = gameObject.GetComponent<EnemyAttackCollider>();
        attackCollider.isAttacking = false;
        // Debug.Log("OnAttackAnimStop");
    }
    public void OnDieAnimStop(){
        Destroy(gameObject);
    }

}
