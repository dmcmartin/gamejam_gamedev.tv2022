using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSystem : MonoBehaviour
{
    public static GameSystem Instance { get; private set; }
    public GameObject player;
    public static bool isPaused = false;
    float originalTimeScale;

    [SerializeField]  GameObject prefabCrawlerTemplate;
    [SerializeField]  GameObject prefabColorfulBee;
    [SerializeField]  GameObject prefabGhost;

    [SerializeField] public GameObject pauseMenuUI;
    [SerializeField] public GameObject instructionMenuUI;
    [SerializeField] public GameObject attributionsMenuUI;

    float minSpawnDistance = 10;
    float maxSpawnDistance = 30;
    float minSpawnTime = 5;
    float maxSpawnTime = 10;


    public const float crawlerYPos = -2.035597f; //TODO: don't have time to make this flexible/generic
//    public const float beeYPos = 1.03f; //TODO: don't have time to make this flexible/generic
    public const float beeYPos = -1.03f; //TODO: don't have time to make this flexible/generic



    void Awake()
    {
        if (Instance != null) {
            Debug.LogError("There is more than one instance!");
            return;
        }

        Instance = this;

        player = GameObject.FindWithTag("Player");

    }

    void Start() {

        // GameObject newEnemy = Instantiate(prefabCrawlerTemplate, new Vector3(10, crawlerYPos,0),Quaternion.identity);
        // GameObject newBeeEnemy = Instantiate(prefabColorfulBee, new Vector3(5, beeYPos,0),Quaternion.identity);

        // newBeeEnemy = Instantiate(prefabColorfulBee, new Vector3(15, beeYPos,0),Quaternion.identity);
        // newEnemy = Instantiate(prefabCrawlerTemplate, new Vector3(10, crawlerYPos,0),Quaternion.identity);


        // Debug.Log("newEnemy"+newEnemy.transform.position);

        SpawnNewEnemy();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            if (GameSystem.isPaused) {
                if (pauseMenuUI.activeSelf)
                {
                    ResumeGame();
                } else {
                    ReturnToPauseMenu();
                }
            } else {
                PauseGame();
            }
        }
    }

    void OnGui()
    {
        // common GUI code goes here
    }
    public void PauseGame() {
        originalTimeScale = Time.timeScale;
        Time.timeScale = 0;
        GameSystem.isPaused = true;
        AudioListener.pause = true;

        pauseMenuUI.SetActive(true);

    }
    public void ResumeGame() {
        Time.timeScale = originalTimeScale;
        GameSystem.isPaused = false;
        AudioListener.pause = false;
        pauseMenuUI.SetActive(false);
    }
    public void ShowInstructionsMenu() {
        pauseMenuUI.SetActive(false);
        instructionMenuUI.SetActive(true);
    }
    public void ReturnToPauseMenu() {
        pauseMenuUI.SetActive(true);
        instructionMenuUI.SetActive(false);
        attributionsMenuUI.SetActive(false);
    }
    void SpawnNewEnemy() {
        float distance = Random.Range(minSpawnDistance,maxSpawnDistance);
        float direction = (Random.value >= 0.5) ? 1.0f : -1.0f;
        float spawnTime = Random.Range(minSpawnTime,maxSpawnTime);
        float playerPos = player.transform.position.x;

        float newPos = playerPos + distance * direction;

        bool spawnBee = (Random.value >= 0.5);
        if (spawnBee){
            GameObject newEnemy = Instantiate(prefabColorfulBee, new Vector3(newPos, beeYPos,0),Quaternion.identity);
        } else {
            GameObject newEnemy = Instantiate(prefabCrawlerTemplate, new Vector3(newPos, crawlerYPos,0),Quaternion.identity);
        }

        Invoke("SpawnNewEnemy", spawnTime);
    }

}