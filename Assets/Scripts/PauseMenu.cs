using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    // [SerializeField] GameObject resumeButton;
    // [SerializeField] GameObject instructionsButton;
    // [SerializeField] GameObject attributionsButton;
    [SerializeField] GameObject quitButton;

    // GameObject [] buttons;
    // int curSelectedButton;

    // Start is called before the first frame update
    void Start()
    {
        //Remove the Quit button on WebGL
        if (Application.platform == RuntimePlatform.WebGLPlayer) {
            quitButton.SetActive(false);
        } else {
            quitButton.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnResumeButton(){
        GameSystem.Instance.ResumeGame();
    }

    public void OnInstructionsButton(){
        gameObject.SetActive(false);
        GameSystem.Instance.instructionMenuUI.SetActive(true);        
    }

    public void OnAttributionsButton(){
        gameObject.SetActive(false);
        GameSystem.Instance.attributionsMenuUI.SetActive(true);        
    }

    public void OnQuitButton(){
        Application.Quit();
    }


}
