using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HitSystem : MonoBehaviour
{

    const int START_HITPOINTS = 3;
    // const int START_HITPOINTS = 1;
    int hitPoints = START_HITPOINTS;


    public UnityEvent OnDeadEvent = new UnityEvent();
    public UnityEvent OnReviveEvent = new UnityEvent();
    public UnityEvent OnHitEvent = new UnityEvent();

    // Start is called before the first frame update
    void Start()
    {
    }

    void Awake() {
        // if (OnDeadEvent == null) {
        //     OnDeadEvent = new UnityEvent();
        // }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnHit(GameObject other) {
        if (hitPoints>0) {
            hitPoints--;
            OnHitEvent.Invoke();
            // Debug.Log("Player OnHit: Health = " + hitPoints);
            if (hitPoints<=0) {
                OnDeadEvent.Invoke();
            }
        }


    }

    public void Revive() {
        //NOTE: This should be called on the player object only...no time to cleanup
        hitPoints = START_HITPOINTS;
        OnReviveEvent.Invoke();
    }



}
