using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This class is used to calculate the rendered bounds of a GameObject
//that contains children
public class BoundsCalculator : MonoBehaviour
{

    //The initial bounds are used to record size of the bounds, the center of the bounds is always calculated on the fly
    Bounds initBounds;

    Vector3 position2CenterOffset; //Used to compensate between the difference of the bounds center and the origin of the container object


    // Start is called before the first frame update
    void Start()
    {
        //Calculate the total bounds dicatated by all children
        Renderer [] childRenderers;
        childRenderers = GetComponentsInChildren<Renderer>();
        if (childRenderers != null)
        {
            initBounds = childRenderers[0].bounds;
            foreach (Renderer r in childRenderers) {
                initBounds.Encapsulate(r.bounds);
            }
        }

        position2CenterOffset = initBounds.center - transform.position;

    }

    public Bounds bounds ()
    {
        Bounds retValue = initBounds;
        retValue.center = position2CenterOffset + transform.position;
        return retValue;
    }

    //Set the transform position relative to the bounds center (not the container object's origin)
    public void SetCenterPosition(Vector3 pos) {

        transform.position = pos - position2CenterOffset;

    }

    //Set the transform X position relative to the bounds center (not the container object's origin)
    public void SetCenterPositionX(float xPos) {

        Vector3 newPos = transform.position;
        newPos.x = xPos - position2CenterOffset.x;
        transform.position = newPos;
    }

    //Set the transform X position relative to the bounds left edge (not the container object's origin)
    public void SetLeftPositionX(float xPos) {

        Vector3 newPos = transform.position;
        newPos.x = xPos - position2CenterOffset.x + initBounds.extents.x;
        transform.position = newPos;
    }



}
