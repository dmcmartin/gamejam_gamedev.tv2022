using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostPlayerController : MonoBehaviour
{
    [SerializeField] GameObject playerObject;
    [SerializeField] ParticleSystem ghostParticles;
    [SerializeField] ParticleSystem ghostParticlesAirse;
    float reviveMoveSpeed = 12f; //Speed of movement to bring ghost back to life
    float speed = 15f;

    bool isReviving=false;  //Ghost returning to player to revive player
    bool isRising=false;    //Player becoming ghost
    bool isMovingToPossess=false; //Ghost is moving towards an enemy to possess
    bool isPossessing=false; //Ghost is possessing an enemy

    float ghostPositionAbovePlayer = 2.0f;
    float ghostRiseSpeed = 2.0f;
    bool facingLeft = true;
    GameObject possessedEnemy;

    float deathDuration = 10.0f;  //Duration that the player is dead and a ghost

    // Start is called before the first frame update
    void Start()
    {
        ghostParticles.Clear();
        ghostParticles.Stop();
        // ghostParticlesAirse.Clear();
        // ghostParticlesAirse.Stop();
        ghostParticlesAirse.Play();
    }

    // Update is called once per frame
    void Update()
    {

        if (isMovingToPossess) {
            MoveToPossessedEnemy();
            return;
        }


        if (isRising) {
            AnimateRiseGhost();
            return;
        }

        if (isReviving) {
            ReviveMoveToPlayer();

            Vector3 vector2Player = playerObject.transform.position - transform.position;
            if (vector2Player.sqrMagnitude <= 0.1) {
                //We've reached the player
                RevivePlayer();
            }


            return;
        }





        if (!isPossessing) {

            //Move Ghost around 
            float movementDirection = Input.GetAxis("Horizontal");
            float moveAmount = movementDirection * speed * Time.deltaTime;
            transform.Translate(moveAmount,0,0);        
            if ( (movementDirection >= 0 && facingLeft) || 
                (movementDirection < 0 && !facingLeft) ) {
                Vector3 newScale = transform.localScale;
                newScale.x *= -1;
                transform.localScale = newScale;
                facingLeft = !facingLeft;
            }


            if (Input.GetKeyDown(KeyCode.Space)) {
                GameObject enemy = FindNearestEnemy();
                if (enemy != null) {
                    possessedEnemy = enemy;
                    isMovingToPossess = true;
                    MoveToPossessedEnemy();
                }
            }



            // if (Input.GetMouseButtonDown(0))
            // {
            //     Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            //     Collider2D targetObject = Physics2D.OverlapPoint(mousePosition);
            //     if (targetObject)
            //     {
            //         // Debug.Log("Clicked on Object");
            //         // Debug.Log("Tag = " + targetObject.transform.parent.gameObject.tag);
            //         if (targetObject.transform.parent.gameObject.tag == "Enemy") {
            //             possessedEnemy = targetObject.transform.parent.gameObject;
            //             isMovingToPossess = true;

            //             // Debug.Log("Clicked on Object");
            //             MoveToPossessedEnemy();



            //         }


            //     }
            // }
        } else {  //if isPossessing
            transform.position = possessedEnemy.transform.position;


//             if (Input.GetKeyDown(KeyCode.Space)) {
// //                ReviveMoveToPlayer();
//                 StartRevival();
//             }


        }


        


    }

    void StartRevival() {
        ReviveMoveToPlayer();

        isReviving = true;
        ghostParticles.Play();

        if (isPossessing) {
            PossessedEnemyController possessedController = possessedEnemy.GetComponent<PossessedEnemyController>();
            possessedController.ReleasePossession();
        }

        SetCameraFollowObject(gameObject);


        isMovingToPossess = false;
        isPossessing = true;
        // gameObject.SetActive(true);
        Renderer rend = GetComponent<Renderer>();
        rend.enabled = true;

    }

    void MoveToPossessedEnemy() {
        Vector3 vector2Enemy = possessedEnemy.transform.position - transform.position;

        // Debug.Log("Move to "+vector2Enemy+ "My Loc="+transform.position + "Enemy Loc="+possessedEnemy.transform.position + "Speed="+speed);

        //Move the ghost towards the enemy
        transform.position = Vector3.MoveTowards(transform.position, possessedEnemy.transform.position, speed * Time.deltaTime);
        // Debug.Log("My NEW Move to "+vector2Enemy+ "My Loc="+transform.position + "Enemy Loc="+possessedEnemy.transform.position + "Speed="+speed);

        if (vector2Enemy.sqrMagnitude <= 0.1) {
            TakePossession();
        }



    }
    void TakePossession() {
        isMovingToPossess = false;
        isPossessing = true;

        // Debug.Log("Ghost to take possession");
        PossessedEnemyController possessedController = possessedEnemy.GetComponent<PossessedEnemyController>();
        possessedController.TakePossession(this);

        SetCameraFollowObject(possessedEnemy);



        // gameObject.SetActive(false);
        Renderer rend = GetComponent<Renderer>();
        rend.enabled = false;

        
    }

    void ReviveMoveToPlayer() {

        //Point the partical system to the player
        var psShape = ghostParticles.shape;

        //TODO: I have no idea why I can't get the correct answer from Vector3.Angle
        //so, I'm calculating the angle directly
        // Vector3 myPos = new Vector3(transform.position.x, transform.position.x, 0);
        // Vector3 playerPos = new Vector3(playerObject.transform.position.x, playerObject.transform.position.x, 0);
        // angle = Vector3.Angle(playerPos,myPos);

        float angle = Mathf.Atan2(playerObject.transform.position.y - transform.position.y,  playerObject.transform.position.x - transform.position.x) * Mathf.Rad2Deg;
        psShape.rotation = new Vector3(0,0,angle);


        Vector3 vector2Player = playerObject.transform.position - transform.position;
        float dist2Player = vector2Player.magnitude;
        var main = ghostParticles.main;
        float lifetime = dist2Player/(main.startSpeed.constant + reviveMoveSpeed);
        main.startLifetime = lifetime;

        //Move the ghost towards the player
        transform.position = Vector3.MoveTowards(transform.position, playerObject.transform.position, reviveMoveSpeed * Time.deltaTime);


    }
    void RevivePlayer() {
        ghostParticles.Clear();
        ghostParticles.Stop();

        SetCameraFollowObject(playerObject);

        HitSystem hitSystem = playerObject.GetComponent<HitSystem>();
        hitSystem.Revive();

        //Delete Ghost
        Destroy(gameObject);

    }
    
    public void RiseGhost(GameObject player) {
        playerObject = player;
        isRising = true;
        // ghostParticlesAirse.Play();

        // Renderer rend = GetComponent<Renderer>();
        // rend.sortingOrder = 100;

        SetCameraFollowObject(gameObject);


        AnimateRiseGhost();
    }

    void AnimateRiseGhost() {
        Vector3 ghostDestPos = new Vector3(playerObject.transform.position.x, playerObject.transform.position.y + ghostPositionAbovePlayer,0);
        transform.position = Vector3.MoveTowards(transform.position, ghostDestPos, ghostRiseSpeed * Time.deltaTime);

        Vector3 vector2GhostDest = ghostDestPos - transform.position;
        if (vector2GhostDest.sqrMagnitude <= 0.1) {
            //We've reached the end of the Rising animation
            isRising = false;
            ghostParticlesAirse.Clear();
            ghostParticlesAirse.Stop();

            //After X seconds, the player will resurrect
            // Debug.Log("Setup of StartRevival");
            Invoke("StartRevival", deathDuration);


        }

        
    }
    void SetCameraFollowObject(GameObject obj){
        GameObject camera = GameObject.FindWithTag("MainCamera");
        FollowCamera followCamer = camera.GetComponent<FollowCamera>();
        followCamer.SetObjectToFollow(obj);
    }

    GameObject FindNearestEnemy(){
        GameObject [] allEnemies = GameObject.FindGameObjectsWithTag("Enemy");
        GameObject closest=null;

        float distance = Mathf.Infinity;
        Vector3 position = transform.position;
        foreach (GameObject enemy in allEnemies)
        {
            Vector3 diff = enemy.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                closest = enemy;
                distance = curDistance;
            }
        }
        return closest;

    }


}
