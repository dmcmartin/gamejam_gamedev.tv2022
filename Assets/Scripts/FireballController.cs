using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballController : MonoBehaviour
{
    float speed = 25f;
    float maxDistance = 15f;
    Vector3 startPos; //Used to destroy fireball if it goes too far
    float direction = 1; //-1 = left, +1 = right
    float totalMovement = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float moveAmount = speed * Time.deltaTime * direction;
        transform.Translate(moveAmount,0,0);      
        totalMovement += Mathf.Abs(moveAmount);


        if (totalMovement > maxDistance) {
            Destroy(gameObject);
            return;
        }


        
    }

    public void Launch(bool facingLeft) {
        Vector3 newScale = transform.localScale;
        newScale.x = Mathf.Abs(newScale.x) * (facingLeft ? 1f : -1f );
        transform.localScale = newScale;
        direction = facingLeft ? -1f : 1f;


        startPos = transform.position;

    }


    private void OnTriggerEnter2D(Collider2D other) {
        // Debug.Log("Fireball Hit, other=" + other+" tag = "+other.tag);

        if (other.tag == "EnemySprite") {

            HitSystem hitSystem = other.transform.parent.GetComponent<HitSystem>();
            hitSystem.OnHit(this.gameObject);


            // HitSystem hitSystem = other.GetComponent<HitSystem>();
            // hitSystem.OnHit(this.gameObject);
            Destroy(gameObject);
        } else if (other.tag == "Untagged") { //This is sooo hacky.
            //We're colliding with the attack collider
            HitSystem hitSystem = other.transform.parent.transform.parent.GetComponent<HitSystem>();
            hitSystem.OnHit(this.gameObject);
            Destroy(gameObject);
        }



    }

}
