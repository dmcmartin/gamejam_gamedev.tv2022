using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{

    [SerializeField] GameObject objectToFollow;
    float prevXPos = -100;


    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void LateUpdate()
    {

        // transform.position = objectToFollow.transform.position + cameraInitPostition;
        // transform.position = objectToFollow.transform.position + new Vector3(0,0,-48.4f);
        Vector3 pos = transform.position;
        pos.x = objectToFollow.transform.position.x;

        if (pos.x != prevXPos) {

            FindObjectOfType<ParallaxScroller>().OnCameraMoved(pos.x);
            transform.position = pos;
            prevXPos = pos.x;

        }


    }
    public void SetObjectToFollow(GameObject obj){
        objectToFollow = obj;
    }


}
