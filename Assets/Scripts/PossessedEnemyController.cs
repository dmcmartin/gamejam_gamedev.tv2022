using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PossessedEnemyController : MonoBehaviour
{
    // bool isPossessed = false;
    GhostPlayerController ghostController;

    public UnityEvent OnTakePossessionEvent = new UnityEvent();
    public UnityEvent OnReleasePossessionEvent = new UnityEvent();
    bool isPossessed = false;


    // Start is called before the first frame update
    void Start()
    {
        // Debug.Log("PossessedEnemyController: start");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TakePossession(GhostPlayerController controller){
        ghostController = controller;
        isPossessed = true;
        // Debug.Log("EnemyController: Possession Taken");
        OnTakePossessionEvent.Invoke();
    }
    public void ReleasePossession(){
        isPossessed = false;
        OnReleasePossessionEvent.Invoke();
    }
    public bool GetIsPossessed(){
        return isPossessed;
    }

}
